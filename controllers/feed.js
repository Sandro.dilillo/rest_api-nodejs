exports.getPosts = (req, res, next) => {
   res.status(200).json({
       posts: [{title: 'First Posts', content: 'this is the first post'}]
   }); //metodo express 
};

exports.createPost = (req, res, next) => {
    const title = req.body.title;
    const content = req.body.content;

    //create post in db
    res.status(201).json({ //una risorsa è stata create = 201
        message: 'Post create successfully',
        post:{ id: new Date().toDateString(), title: title, content: content} 
    });
};